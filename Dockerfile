FROM adoptopenjdk/openjdk11:ubi
LABEL maintainer="Stanislav Migunov 'stanislav.migunov@outlook.com'"
EXPOSE 8080

COPY . /java-app
RUN cd java-app && ./gradlew build
ENTRYPOINT ["/java-app/entrypoint.bash"]
