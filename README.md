![Иллюстрация к тестовому заданию](https://i.ibb.co/Pt5pbqt/Screenshot-2021-02-20-at-19-55-30.png)

# Тестовое задание 
    - Описание: 
        SpringBoot приложение, которое оперирует над данными несовершенного API (https://github.com/dmitry093/test-task)
    - Запуск: 
        Для запуска всех необходимых ресурсов используем Docker, 
        $ docker-compose up -d 
    - Ресурсы: 
        * http://localhost:8080 - New Car Api
        * htpp://localhost:8084 - Old Car Api

