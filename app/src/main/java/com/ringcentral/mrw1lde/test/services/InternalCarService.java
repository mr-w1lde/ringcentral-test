package com.ringcentral.mrw1lde.test.services;

import com.ringcentral.mrw1lde.test.models.AbstractCarInfo;
import com.ringcentral.mrw1lde.test.models.CarFullInfo;
import com.ringcentral.mrw1lde.test.models.external.ExternalBrand;
import com.ringcentral.mrw1lde.test.models.external.ExternalCar;
import com.ringcentral.mrw1lde.test.models.external.ExternalCarInfo;
import com.ringcentral.mrw1lde.test.models.external.enums.EngineType;
import com.ringcentral.mrw1lde.test.models.external.enums.FuelType;
import com.ringcentral.mrw1lde.test.models.external.enums.GearboxType;
import com.ringcentral.mrw1lde.test.models.external.enums.WheelDriveType;
import com.ringcentral.mrw1lde.test.models.params.CarRequestParameters;
import com.ringcentral.mrw1lde.test.models.params.MaxSpeedRequestParameters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class InternalCarService {
    private static final Logger LOGGER = LogManager.getLogger(InternalCarService.class);

    @Autowired
    private ExternalCarsApiService externalCarsApiService;

    private final List<CarFullInfo> CAR_FULL_INFO_LIST = new ArrayList<>();

    private final Set<String> BODY_STYLE_SET = new HashSet<>();
    private final Set<FuelType> FUEL_TYPE_SET = new HashSet<>();
    private final Set<EngineType> ENGINE_TYPE_SET = new HashSet<>();
    private final Set<WheelDriveType> WHEEL_DRIVE_TYPE_SET = new HashSet<>();
    private final Set<GearboxType> GEARBOX_TYPE_SET = new HashSet<>();

    @PostConstruct
    private void init() {
        LOGGER.info("Parsing External Car Api...");
        long startTime = System.currentTimeMillis();

        List<ExternalCar> externalCarList = externalCarsApiService.loadAllCars();
        List<ExternalBrand> externalBrandList = externalCarsApiService.loadAllBrands();

        externalCarList.forEach(t -> {
            ExternalCarInfo externalCarInfo = externalCarsApiService.loadCarInformationById(t.getId());
            CarFullInfo carFullInfo = CarFullInfo.fromExternal(externalCarInfo, externalBrandList.get(t.getBrandId() - 1));

            CAR_FULL_INFO_LIST.add(carFullInfo);
            BODY_STYLE_SET.addAll(carFullInfo.getBodyCharacteristics().getBodyStyle());
            FUEL_TYPE_SET.add(carFullInfo.getEngineCharacteristics().getFuelType());
            ENGINE_TYPE_SET.add(carFullInfo.getEngineCharacteristics().getEngineType());
            WHEEL_DRIVE_TYPE_SET.add(carFullInfo.getWheelDriveType());
            GEARBOX_TYPE_SET.add(carFullInfo.getGearboxType());
        });

        long endTime = System.currentTimeMillis() - startTime;
        LOGGER.info("Done! Parsed for {} ms.", endTime);
    }

    public ResponseEntity<List<? extends AbstractCarInfo>>  getCars(CarRequestParameters requestParameters) {
        Stream<CarFullInfo> filteredStream = CAR_FULL_INFO_LIST.stream();

        //Country Filter
        if(StringUtils.hasText(requestParameters.getCountry())) {
            filteredStream = filteredStream
                    .filter(t -> t.getCar().getCountry().equals(requestParameters.getCountry()));
        }

        //Segment Filter
        if(StringUtils.hasText(requestParameters.getSegment())) {
            filteredStream = filteredStream
                    .filter(t -> t.getCar().getSegment().equals(requestParameters.getSegment()));
        }

        //MinEngineDisplacement Filter
        if(requestParameters.getMinEngineDisplacement() != null) {
            filteredStream = filteredStream
                    .filter(t -> t.getEngineCharacteristics()
                            .getEngineDisplacement() >= requestParameters.getMinEngineDisplacement());
        }

        //MinEngineHorsepower Filter
        if(requestParameters.getMinEngineHorsepower() != null) {
            filteredStream = filteredStream
                    .filter(t -> t.getEngineCharacteristics().getHp() >= requestParameters.getMinEngineHorsepower());
        }

        //MinMaxSpeed Filter
        if(requestParameters.getMinMaxSpeed() != null) {
            filteredStream = filteredStream
                    .filter(t -> t.getMaxSpeed() >= requestParameters.getMinMaxSpeed());
        }

        //BodyStyle Filter
        if(StringUtils.hasText(requestParameters.getBodyStyle())) {
            filteredStream = filteredStream
                    .filter(t -> t.getBodyCharacteristics().getBodyStyle().contains(requestParameters.getBodyStyle()));
        }

        //Year Filter
        if(requestParameters.getYear() != null) {
            filteredStream = filteredStream.filter(t -> {
                List<Integer> range = t.getYearsRange();
                return requestParameters.getYear() >= range.get(0) && requestParameters.getYear() <= range.get(1);
            });
        }

        //Search Filter
        if(StringUtils.hasText(requestParameters.getSearch())) {
            filteredStream = filteredStream.filter(t ->
                            t.getCar().getModel().contains(requestParameters.getSearch()) ||
                            t.getCar().getGeneration().contains(requestParameters.getSearch()) ||
                            t.getCar().getModification().contains(requestParameters.getSearch())
            );
        }

        List<? extends AbstractCarInfo> result;

        //isFull Filter
        if(requestParameters.getIsFull() != null && requestParameters.getIsFull().equals(true)) {
            result = filteredStream.collect(Collectors.toList());
        } else {
            result = filteredStream.map(CarFullInfo::getCar).collect(Collectors.toList());
        }

        return ResponseEntity.ok(result);
    }

    public List<String> getFuelTypes() {
        return FUEL_TYPE_SET.stream()
                .map(Enum::toString)
                .collect(Collectors.toList());
    }

    public List<String> getBodyStyles() {
        return new ArrayList<>(BODY_STYLE_SET);
    }

    public List<String> getEngineTypes() {
        return ENGINE_TYPE_SET.stream()
                .map(Enum::toString)
                .collect(Collectors.toList());
    }

    public List<String> getWheelDrives() {
        return WHEEL_DRIVE_TYPE_SET.stream()
                .map(Enum::toString)
                .collect(Collectors.toList());
    }

    public List<String> getGearboxTypes() {
        return GEARBOX_TYPE_SET.stream()
                .map(Enum::toString)
                .collect(Collectors.toList());
    }

    public double getMaxSpeed(MaxSpeedRequestParameters requestParameters) {
        List<CarFullInfo> carFullInfoFiltered = Collections.emptyList();

        if(StringUtils.hasText(requestParameters.getModel())) {
            carFullInfoFiltered = CAR_FULL_INFO_LIST.stream()
                    .filter(t -> t.getCar().getModel().equals(requestParameters.getModel()))
                    .collect(Collectors.toList());
        } else if(StringUtils.hasText(requestParameters.getBrand())) {
            carFullInfoFiltered = CAR_FULL_INFO_LIST.stream()
                    .filter(t -> t.getCar().getBrand().equals(requestParameters.getBrand()))
                    .collect(Collectors.toList());
        }

        return carFullInfoFiltered.stream()
                .mapToInt(CarFullInfo::getMaxSpeed)
                .average()
                .orElse(0.0);
    }


}
