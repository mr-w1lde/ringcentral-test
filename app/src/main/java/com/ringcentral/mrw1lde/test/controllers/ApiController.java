package com.ringcentral.mrw1lde.test.controllers;

import com.ringcentral.mrw1lde.test.models.AbstractCarInfo;
import com.ringcentral.mrw1lde.test.models.CarInfo;
import com.ringcentral.mrw1lde.test.models.params.CarRequestParameters;
import com.ringcentral.mrw1lde.test.models.params.MaxSpeedRequestParameters;
import com.ringcentral.mrw1lde.test.services.InternalCarService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

import static java.util.Collections.emptyList;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ApiController {

    @Autowired
    private InternalCarService internalCarService;

    @GetMapping("/cars")
    public ResponseEntity<List<? extends AbstractCarInfo>> getCar(CarRequestParameters requestParameters) {
        return internalCarService.getCars(requestParameters);
    }

    @GetMapping("/fuel-types")
    public ResponseEntity<List<String>> getFuelTypes() {
        return ResponseEntity.ok(internalCarService.getFuelTypes());
    }

    @GetMapping("/body-styles")
    public ResponseEntity<List<String>> getBodyStyles() {
        return ResponseEntity.ok(internalCarService.getBodyStyles());
    }

    @GetMapping("/engine-types")
    public ResponseEntity<List<String>> getEngineTypes() {
        return ResponseEntity.ok(internalCarService.getEngineTypes());
    }

    @GetMapping("/wheel-drives")
    public ResponseEntity<List<String>> getWheelDrives() {
        return ResponseEntity.ok(internalCarService.getWheelDrives());
    }

    @GetMapping("/gearboxes")
    public ResponseEntity<List<String>> getGearboxTypes() {
        return ResponseEntity.ok(internalCarService.getGearboxTypes());
    }

    @GetMapping("/max-speed")
    public ResponseEntity<Double> getMaxSpeed(MaxSpeedRequestParameters requestParameters) {
        if(!StringUtils.hasText(requestParameters.getBrand()) && !StringUtils.hasText(requestParameters.getModel())) {
            return ResponseEntity.notFound().build();
        }

        if(StringUtils.hasText(requestParameters.getBrand()) && StringUtils.hasText(requestParameters.getModel())) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(internalCarService.getMaxSpeed(requestParameters));
    }
}