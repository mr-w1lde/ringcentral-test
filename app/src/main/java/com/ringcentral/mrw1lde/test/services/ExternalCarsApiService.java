package com.ringcentral.mrw1lde.test.services;

import com.ringcentral.mrw1lde.test.models.external.ExternalBrand;
import com.ringcentral.mrw1lde.test.models.external.ExternalCar;
import com.ringcentral.mrw1lde.test.models.external.ExternalCarInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class ExternalCarsApiService {
    private static final Logger LOGGER = LogManager.getLogger(ExternalCarsApiService.class);

    @Autowired
    private Environment environment;

    private String ALL_CARS_URL;
    private String CAR_BY_ID_URL;
    private String ALL_BRANDS_URL;

    private final RestTemplate restTemplate = new RestTemplate();

    @PostConstruct
    private void init() {
        String API_HOST = environment.getProperty("API_HOST", "http://localhost:8084/%s");

        ALL_CARS_URL = String.format(API_HOST, "api/v1/cars");
        CAR_BY_ID_URL = String.format(API_HOST, "api/v1/cars/%d");
        ALL_BRANDS_URL = String.format(API_HOST, "api/v1/brands");
    }

    public List<ExternalCar> loadAllCars() {
        try {
            ResponseEntity<ExternalCar[]> allCarsResponse = restTemplate.getForEntity(ALL_CARS_URL, ExternalCar[].class);
            if (allCarsResponse.getStatusCode() != HttpStatus.OK || allCarsResponse.getBody() == null) {
                return Collections.emptyList();
            }
            return Arrays.asList(allCarsResponse.getBody());
        } catch (RestClientException restClientException) {
            LOGGER.error("Error when trying to load all cars", restClientException);
            return Collections.emptyList();
        }
    }

    public ExternalCarInfo loadCarInformationById(int id) {
        String carUrl = String.format(CAR_BY_ID_URL, id);
        try {
            ResponseEntity<ExternalCarInfo> carInfoResponse = restTemplate.getForEntity(carUrl, ExternalCarInfo.class);
            if (carInfoResponse.getStatusCode() != HttpStatus.OK || carInfoResponse.getBody() == null) {
                return null;
            }
            return carInfoResponse.getBody();
        } catch (RestClientException restClientException) {
            LOGGER.error("Error when trying to load car with id {}", id, restClientException);
            return null;
        }
    }

    public List<ExternalBrand> loadAllBrands() {
        try {
            ResponseEntity<ExternalBrand[]> allBrandsResponse = restTemplate.getForEntity(ALL_BRANDS_URL, ExternalBrand[].class);
            if (allBrandsResponse.getStatusCode() != HttpStatus.OK || allBrandsResponse.getBody() == null) {
                return Collections.emptyList();
            }
            return Arrays.asList(allBrandsResponse.getBody());
        } catch (RestClientException restClientException) {
            LOGGER.error("Error when trying to load all brands", restClientException);
            return Collections.emptyList();
        }
    }
}