package com.ringcentral.mrw1lde.test.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ringcentral.mrw1lde.test.models.external.ExternalBrand;
import com.ringcentral.mrw1lde.test.models.external.ExternalCarInfo;
import com.ringcentral.mrw1lde.test.models.external.enums.GearboxType;
import com.ringcentral.mrw1lde.test.models.external.enums.WheelDriveType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CarFullInfo implements AbstractCarInfo {
    @JsonProperty("car_base")
    private CarInfo car;

    @JsonProperty("body")
    private BodyCharacteristics bodyCharacteristics;

    @JsonProperty("engine")
    private EngineCharacteristics engineCharacteristics;

    @JsonProperty("year_range")
    private List<Integer> yearsRange;

    @JsonProperty("gearbox")
    private GearboxType gearboxType;

    @JsonProperty("wheel_drive")
    private WheelDriveType wheelDriveType;

    @JsonProperty("acceleration")
    private Double acceleration;

    @JsonProperty("max_speed")
    private Integer maxSpeed;

    public static CarFullInfo fromExternal(ExternalCarInfo externalCarInfo, ExternalBrand externalBrand) {
        if (externalCarInfo == null) {
            return null;
        }

        CarInfo carInfo = new CarInfo();

        carInfo.setId(externalCarInfo.getId());
        carInfo.setSegment(externalCarInfo.getSegment());
        carInfo.setBrand(externalBrand.getTitle());
        carInfo.setModel(externalCarInfo.getModel());
        carInfo.setCountry(externalBrand.getCountry());
        carInfo.setGeneration(externalCarInfo.getGeneration());
        carInfo.setModification(externalCarInfo.getModification());

        BodyCharacteristics bodyCharacteristics = new BodyCharacteristics();

        bodyCharacteristics.setBodyHeight(externalCarInfo.getBodyHeight());
        bodyCharacteristics.setBodyWidth(externalCarInfo.getBodyWidth());
        bodyCharacteristics.setBodyLength(externalCarInfo.getBodyLength());
        bodyCharacteristics.setBodyStyle(Arrays.asList(externalCarInfo.getBodyStyle().split(",  ")));

        EngineCharacteristics engineCharacteristics = new EngineCharacteristics();

        engineCharacteristics.setEngineType(externalCarInfo.getEngineType());
        engineCharacteristics.setEngineDisplacement(externalCarInfo.getEngineDisplacement());
        engineCharacteristics.setFuelType(externalCarInfo.getFuelType());
        engineCharacteristics.setHp(externalCarInfo.getHp());

        CarFullInfo carFullInfo = new CarFullInfo();

        carFullInfo.setCar(carInfo);
        carFullInfo.setBodyCharacteristics(bodyCharacteristics);
        carFullInfo.setEngineCharacteristics(engineCharacteristics);

        String[] years = externalCarInfo.getYearsRange().split("-").clone();

        int first_rang = Integer.parseInt(years[0]);
        int second_rang = years[1].equals("present")
                ? Calendar.getInstance().get(Calendar.YEAR) : Integer.parseInt(years[1]);

        carFullInfo.setYearsRange(Arrays.asList(first_rang, second_rang));
        carFullInfo.setGearboxType(externalCarInfo.getGearboxType());
        carFullInfo.setWheelDriveType(externalCarInfo.getWheelDriveType());
        carFullInfo.setAcceleration(externalCarInfo.getAcceleration());
        carFullInfo.setMaxSpeed(externalCarInfo.getMaxSpeed());

        return carFullInfo;
    }
}
