package com.ringcentral.mrw1lde.test.models.external.enums;

public enum WheelDriveType {
    AWD, FWD, RWD
}
