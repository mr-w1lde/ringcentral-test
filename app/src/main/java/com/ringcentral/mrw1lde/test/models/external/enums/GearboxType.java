package com.ringcentral.mrw1lde.test.models.external.enums;

public enum GearboxType {
    AUTO, MANUAL, ROBOTIC, CVT
}