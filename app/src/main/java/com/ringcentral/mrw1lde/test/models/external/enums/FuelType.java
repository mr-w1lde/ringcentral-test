package com.ringcentral.mrw1lde.test.models.external.enums;

public enum FuelType {
    GASOLINE, DIESEL, HYBRID
}