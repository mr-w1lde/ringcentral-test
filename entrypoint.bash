#!/bin/bash

set -e
host="old-api"
port="8080"

echo "!!!!!!!! Check old-api for available !!!!!!!!"

until curl http://"$host":"$port"/api/v1/cars/1; do
    echo "old-api is unavailable - sleeping"
    sleep 6
done

echo "old-api is up - executing command"
java -jar /java-app/app/build/libs/app-1.1.0.jar -Xmx1024m -API_HOST="${API_HOST}"